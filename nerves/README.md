# nervesShell

This package creates an environment with the requirements for Nerves. It is
extracted from the [Nerves Installation guide](https://hexdocs.pm/nerves/installation.html#for-nixos-or-nix-package-manager),
although I don't promise it remains the same as the original, because it is possible
that I need to add something else for my own usage.

Before to use this one, check the commits of this `shell.nix` file to know if there
is some change from the original or directly compare the original with this one.

In case you miss or your are new, if you want to run it you need to:

```
nix-shell
```

Or if you change the name of the file:

```
nix-shell <FILENAME>.nix
```
