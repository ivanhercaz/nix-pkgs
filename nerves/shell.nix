{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  name = "nervesShell";
  buildInputs = [
    autoconf
    automake
    curl
    erlangR25
    fwup
    git
    pkgs.beam.packages.erlangR25.elixir
    rebar3
    squashfsTools
    x11_ssh_askpass
  ];
  shellHook = ''
    echo "Welcome to NervesShell built with a nix.conf"
    export SUDO_ASKPASS=${pkgs.x11_ssh_askpass}/libexec/x11-ssh-askpass
  '';
}
